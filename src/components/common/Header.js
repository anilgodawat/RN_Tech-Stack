import React from 'react';

import { Text, View, SafeAreaView } from 'react-native';

const Header = props => {
  const { textStyle, viewStyle } = styles;
  return (
    <SafeAreaView>
      <View style={viewStyle}>
        <Text style={textStyle}>{props.name}</Text>
      </View>
    </SafeAreaView>
  );
};

export { Header };

const styles = {
  viewStyle: {
    backgroundColor: '#F7F5F7',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    paddingTop: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2.0 },
    shadowOpacity: 0.2
  },
  textStyle: {
    fontSize: 20
  }
};
