import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
  LayoutAnimation
} from 'react-native';
import { CardSection } from './common';
import { selectLibrary } from '../actions';
import { connect } from 'react-redux';

class ListItem extends Component {
  componentWillUpdate() {
    LayoutAnimation.spring();
  }
  rednerDescription() {
    const { library, expanded } = this.props;
    if (expanded) {
      return (
        <CardSection>
          <Text style={{ flex: 1 }}> {library.description} </Text>
        </CardSection>
      );
    }
  }
  render() {
    console.log(this.props);
    const { textStyle } = styles;
    const { id, title } = this.props.library;
    return (
      <TouchableWithoutFeedback onPress={() => this.props.selectLibrary(id)}>
        <View>
          <CardSection>
            <Text style={textStyle}> {title} </Text>
          </CardSection>
          {this.rednerDescription()}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  textStyle: {
    fontSize: 20,
    paddingLeft: 15,
    paddingRight: 15
  }
};

const mapStateToProps = (state, ownProps) => {
  const expanded = state.selectedLibraryId == ownProps.library.id;
  return { expanded };
};

export default connect(
  mapStateToProps,
  { selectLibrary }
)(ListItem);
