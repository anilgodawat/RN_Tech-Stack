import React from 'react';
import { AppRegistery } from 'react-native';
import App from './App';

AppRegistery.registerComponent('tech-stack', () => App);
